const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect(
	'mongodb+srv://marynas:sMaM8orQbiCj18ol@marynascluster.lwungkp.mongodb.net/notes?retryWrites=true&w=majority'
);

const { authRouter } = require('./authRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { notesRouter } = require('./notesRouter.js');
const { authMiddleware } = require('./middleware/authMiddleware.js');
	
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth/', authRouter);
app.use('/api/users/me', authMiddleware, usersRouter);
app.use('/api/notes/', authMiddleware, notesRouter);

const start = async () => {
	try {
		app.listen(8080);
	} catch (err) {
		console.error(`Error on server startup: ${err.message}`);
	}
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
	console.error(err);
	res.status(500).send({ message: 'Server error' });
}
