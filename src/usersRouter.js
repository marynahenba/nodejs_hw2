const express = require('express');
const router = express.Router();
const {
	getUser,
	deleteUser,
	changeUserPassword,
} = require('./usersService.js');

router.get('/', getUser);

router.delete('/', deleteUser);

router.patch('/', changeUserPassword);

module.exports = {
	usersRouter: router,
};
