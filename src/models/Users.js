const mongoose = require('mongoose');

const User = mongoose.model('User', {
	username: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	createdDate: {
		type: String,
		default: new Date().toString(),
	},
});

module.exports = {
	User,
};
