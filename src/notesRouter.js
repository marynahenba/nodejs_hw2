const express = require('express');
const router = express.Router();
const {
	addNote,
	getNotes,
	getNoteById,
	updateNoteById,
	changeNoteCompletionById,
	deleteNoteById,
} = require('./notesService.js');

router.get('/', getNotes);

router.post('/', addNote);

router.get('/:id', getNoteById);

router.put('/:id', updateNoteById);

router.patch('/:id', changeNoteCompletionById);

router.delete('/:id', deleteNoteById);

module.exports = {
	notesRouter: router,
};
