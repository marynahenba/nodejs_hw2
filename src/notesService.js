const { Note } = require('./models/Notes.js');

async function addNote(req, res, next) {
	try {
		const { text } = req.body;
		if (text) {
			const note = new Note({
				text,
				userId: req.user._id,
			});
			await note.save();
			return res.status(200).send({ message: 'Success' });
		} else {
			return res.status(400).send({ message: 'Parameter "text" is reqiured' });
		}
	} catch (err) {
		next(err);
	}
}

async function getNotes(req, res, next) {
	try {
		const result = await Note.find({ userId: req.user._id }, '-__v');
		if (result) {
			return res.status(200).send({ message: 'Success', notes: result });
		} else {
			return res.status(400).send({ message: 'Client error' });
		}
	} catch (err) {
		next(err);
	}
}

async function getNoteById(req, res, next) {
	try {
		const note = await Note.findById(req.params.id);
		if (note) {
			return res.status(200).send({ note });
		} else {
			return res
				.status(400)
				.send({ message: `Note with id ${req.params.id} is not found` });
		}
	} catch (err) {
		next(err);
	}
}

function updateNoteById(req, res, next) {
	try {
		const { text } = req.body;
		if (text) {
			return Note.findByIdAndUpdate(
				{ _id: req.params.id },
				{
					$set: { text },
				}
			).then(() => {
				res.status(200).send({ message: 'Success' });
			});
		} else {
			return res
				.status(400)
				.send({ message: 'Please, specify text papameter' });
		}
	} catch (err) {
		next(err);
	}
}

function deleteNoteById(req, res, next) {
	try {
		Note.findByIdAndDelete(req.params.id).then((user) => {
			res.status(200).send({ message: 'Success' });
		});
	} catch (err) {
		next(err);
	}
}

function changeNoteCompletionById(req, res, next) {
	try {
		Note.findById(req.params.id).then(async (note) => {
			const checkBox = !note.completed;
			const message =
				note.completed === false ? 'Note was checked' : 'Note was unchecked';
			await Note.findByIdAndUpdate(
				{ _id: req.params.id },
				{ $set: { completed: checkBox } }
			);
			res.status(200).send({ message });
		});
	} catch (err) {
		next(err);
	}
}

module.exports = {
	addNote,
	getNotes,
	getNoteById,
	updateNoteById,
	deleteNoteById,
	changeNoteCompletionById,
};
