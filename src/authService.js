const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const secret = 'secret-jwt-key';

async function register(req, res, next) {
	try {
		const { username, password } = req.body;
		if (username && password) {
			const user = new User({
				username,
				password: await bcrypt.hash(password, 10),
			});
			await user.save();
			return res.status(200).send({ message: 'Success' });
		} else {
			return res
				.status(400)
				.send({ message: 'Please, specify correct username and password' });
		}
	} catch (err) {
		next(err);
	}
}

async function login(req, res, next) {
	try {
		const user = await User.findOne({ username: req.body.username });
		if (
			user &&
			(await bcrypt.compare(String(req.body.password), String(user.password)))
		) {
			const payload = {
				_id: user._id,
				username: user.username,
				createdDate: user.createdDate,
			};
			const jwtToken = jwt.sign(payload, secret);
			return res.status(200).send({ message: 'Success', jwt_token: jwtToken });
		}
		return res.status(400).json({ message: 'Not authorized' });
	} catch (err) {
		next(err);
	}
}

module.exports = {
	register,
	login,
};
