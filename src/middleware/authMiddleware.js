const jwt = require('jsonwebtoken');
const secret = 'secret-jwt-key';

const authMiddleware = (req, res, next) => {
	const { authorization } = req.headers;

	if (!authorization) {
		return res
			.status(401)
			.json({ message: 'Please, provide authorization header' });
	}

	const [, token] = authorization.split(' ');

	if (!token) {
		return res
			.status(401)
			.json({ message: 'Please, include token to request' });
	}

	try {
		const tokenPayload = jwt.verify(token, secret);
		req.user = {
			_id: tokenPayload._id,
			username: tokenPayload.username,
			createdDate: tokenPayload.createdDate,
		};
		next();
	} catch (err) {
		return res.status(401).json({ message: err.message });
	}
};

module.exports = {
	authMiddleware,
};
