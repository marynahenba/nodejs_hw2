const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');

function getUser(req, res, next) {
	try {
		if (req.user) {
			return res.status(200).send({ user: req.user });
		} else {
			return res.status(400).send({ message: 'Something went wrong' });
		}
	} catch (err) {
		next(err);
	}
}

function deleteUser(req, res, next) {
	try {
		User.findByIdAndDelete(req.user._id).then((user) => {
			res.status(200).send({ message: 'Success' });
		});
	} catch (err) {
		next(err);
	}
}

async function changeUserPassword(req, res, next) {
	try {
		const user = await User.findById(req.user._id);
		const { oldPassword, newPassword } = req.body;
		if (user && bcrypt.compare(String(oldPassword), String(user.password))) {
			hachedPasssword = await bcrypt.hash(newPassword, 10);
			user.password = hachedPasssword;
			await user.save();
			return res.status(200).send({ message: 'Password updated' });
		} else {
			return res.status(400).send({ message: 'Wrong password' });
		}
	} catch (err) {
		next(err);
	}
}

module.exports = {
	getUser,
	deleteUser,
	changeUserPassword,
};
